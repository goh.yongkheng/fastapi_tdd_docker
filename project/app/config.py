# Define environment specific configuration variables here

import logging
import os
from functools import lru_cache

from pydantic import BaseSettings, AnyUrl

log = logging.getLogger(__name__)


class Settings(BaseSettings):
    # possible environmnet values are: "dev", "stage", "prod"
    environment: str = os.getenv("ENVIRONMENT", "dev")
    # testing variable defines if we are currently in test mode
    testing: bool = os.getenv("TESTING", 0)
    database_url: AnyUrl = os.environ.get("DATABASE_URL")


@lru_cache()  # adding lru_cache to cache the environment variables to speed up web app
def get_settings() -> BaseSettings:
    log.info("Loading config setting from the environment ...")
    return Settings()
