from app import main


def test_ping(test_app):
    # Given-When-Then framework
    # Given: test_app

    # When: receive response
    response = test_app.get("/ping")

    # Then: list of asserts
    assert response.status_code == 200
    assert response.json() == {"environment": "dev", "ping": "pong", "testing": True}
